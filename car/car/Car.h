#include <string>
#include <iostream>


namespace Cars
{
	class Car
	{
	private:
		static uint64_t s_currentCarPlate;

		const int MIN_YEAR = 1900;
		const int MAX_ENGINE_VOLUME = 10000;

		std::string m_make;
		std::string m_model;
		size_t m_year;
		std::string m_color;
		std::string m_carPlate;
	protected:
		size_t m_KMCapacety;
	public:
		Car();
		Car(std::string make, std::string model, size_t year, size_t engineVolume, std::string color);
		Car(Car &car);
		void ToString() const;
		const Car& CmpByYear(const Car& car2) const;
		const Car& CmpByCarPlate(const Car& car2) const;
		const Cars::Car& Cars::Car::CmpByDrivingRange(const Car& car2) const;
	};

	class FuelCar : public Car
	{
		size_t m_fualTankCapacity;
		size_t m_KML;
	public:
		FuelCar(size_t m_fualTankCapacity, size_t m_KML, std::string make,
			std::string model, size_t year, std::string color);
	};

	class ElectricCar : public Car
	{
		size_t m_batteryCapacity;
		size_t m_KME;
	public:
		ElectricCar(size_t m_batteryCapacity, size_t m_KME, std::string make,
			std::string model, size_t year, std::string color);
	};
}