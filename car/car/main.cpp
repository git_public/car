#include "Car.h"


int main()
{
	std::string make, model, color;
	size_t year, engineVolume;
	std::cout << "insert make(string): ";
	std::cin >> make;
	std::cout << "insert model(string): ";
	std::cin >> model;
	std::cout << "insert year(uint): ";
	std::cin >> year;
	std::cout << "insert engineVolume(uint): ";
	std::cin >> engineVolume;
	std::cout << "insert color(string): ";
	std::cin >> color;

	Cars::Car c(make, model, year, engineVolume, color);
	c.ToString();
	Cars::Car d(make, model, year + 2, engineVolume + 5, color);
	Cars::Car e(make, model, year + 4, engineVolume - 5, color);
	d.CmpByDrivingRange(e).ToString();
	d.CmpByYear(e).ToString();

	const Cars::Car f(make, model, year, engineVolume, color);
	f.ToString();
	const Cars::Car g(make, model, year + 2, engineVolume + 5, color);
	const Cars::Car h(make, model, year + 4, engineVolume - 5, color);
	h.CmpByDrivingRange(g).ToString();
	h.CmpByYear(g).ToString();
	h.CmpByCarPlate(g).ToString();

	Cars::ElectricCar ec(100, 7, make, model, year, color);
	Cars::FuelCar fc(55, 10, make, model, year, color);
	ec.CmpByDrivingRange(fc).ToString();
	ec.ToString();
	fc.ToString();

	return 0;
}
