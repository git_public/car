#include "Car.h"

Cars::Car::Car()
{
	//test ctor
	m_make = "Ford";
	m_model = "Focus";
	m_year = 2017;
	m_KMCapacety = 600;
	m_color = "White";
}

Cars::Car::Car(std::string make, std::string model, size_t year, size_t KMCapacety, std::string color)
{
	if(make == "" || model == "" || color == "" || year < MIN_YEAR || MAX_ENGINE_VOLUME > 10000)
	{
		std::cout << "unvalid data";
		return;
	}
	m_make = make;
	m_model = model;
	m_year = year;
	m_KMCapacety = KMCapacety;
	m_color = color;
	m_carPlate = std::to_string(Car::s_currentCarPlate);
	Car::s_currentCarPlate++;
}

Cars::Car::Car(Car &car) :
	m_make(car.m_make), 
	m_model(car.m_model),
	m_year(car.m_year),
	m_KMCapacety(car.m_KMCapacety),
	m_color(car.m_color),
	m_carPlate(car.m_carPlate){	}

void Cars::Car::ToString() const
{
	std::string str = "Make: " + m_make + "\tModel: " + m_model + "\tYear: " + std::to_string(m_year) +
		"\tKM Capacety: " + std::to_string(m_KMCapacety) + "\tColor: " + m_color + "\tCar Plate: " + m_carPlate + "\r\n" ;
	std::cout << str;
}

const Cars::Car& Cars::Car::CmpByYear(const Car& car2) const
{
	if(m_year >= car2.m_year)
	{
		return *this;
	}
	else
	{
		return car2;
	}
}

const Cars::Car& Cars::Car::CmpByDrivingRange(const Car& car2) const
{
	if(m_KMCapacety >= car2.m_KMCapacety)
	{
		return *this;
	}
	else
	{
		return car2;
	}
}

const Cars::Car& Cars::Car::CmpByCarPlate(const Car& car2) const
{
	if(std::stoull(m_carPlate) >= std::stoull(car2.m_carPlate))
	{
		return *this;
	}
	else
	{
		return car2;
	}
}

uint64_t Cars::Car::s_currentCarPlate = 10000001;

Cars::FuelCar::FuelCar(size_t m_fualTankCapacity, size_t m_KML, std::string make,
	std::string model, size_t year, std::string color) :
	Car(make, model, year, m_fualTankCapacity*m_KML, color),
	m_fualTankCapacity(m_fualTankCapacity),
	m_KML(m_KML)
{

}

Cars::ElectricCar::ElectricCar(size_t m_batteryCapacity, size_t m_KME, std::string make,
	std::string model, size_t year, std::string color) :
	Car(make, model, year, m_batteryCapacity*m_KME, color),
	m_batteryCapacity(m_batteryCapacity),
	m_KME(m_KME)
{

}